set collection_result_display_limit -1 
set search_path [list . <path_to_folder_containing_.lib_and_.db_files> ]
set target_library [list <library.db>]
set link_library  [list * <library.db>]


set rtl_image [list ~/verilog_folder_path/filename1.v ~/verilog_folder_path/filename2.v]

#set_host_options -max_cores 4

define_design_lib work -path ./Synthesis/Work


set hdlin_check_no_latch true
set hdlin_report_syn_cell true ; # no man entry for this command. Need to check
set hdlin_report_inferred_modules verbose
set hlo_resource_allocation "constraint_driven"

#set report_default_significant_digits [getenv SIGDIGIT];

# RCFNL
#set_flatten true -minimize single_output -phase true;

set auto_wire_load_selection true
set compile_delete_unloaded_sequential_cells "false"
set compile_seqmap_enable_output_inversion "false"
set compile_seqmap_propagate_constants "false"
set compile_seqmap_propagate_high_effort "false"
set timing_enable_multiple_clocks_per_reg "true"
set compile_dont_use_dedicated_scanout 1
set enable_recovery_removal_arcs "false"
set compile_implementation_selection "true"
set write_name_nets_same_as_ports true
set verilogout_higher_designs_first true
set verilogout_no_tri true
set report_default_significant_digits 4
redirect analyze.log {analyze -f verilog $rtl_image} -tee

#set_wire_load_model -name "wl0"
#set wire_load_mode "top"
set hdlin_no_group_register true
# -----------------------------------------------------------------------------
# Elaborate the Design
# -----------------------------------------------------------------------------

redirect .elaborate.log {elaborate -architecture verilog TopModule } -tee 

current_design TopModule
link   

set_isolate_ports -type buffer [all_outputs]
# RCFNL
set_fix_multiple_port_nets -feedthroughs -outputs -constants [get_designs *];

# RCFNL
#set_ultra_optimization true
set_structure -boolean true -boolean_effort high -timing true;

uniquify -dont_skip_empty_designs  

  # Set constraints and mapping on target library



  set sub_clocks  {CLK}
  set sub_resets  {RST_N}
  
create_clock  -name CLK -period 0.500 [get_ports $sub_clocks] 

set_output_delay 0.100 -max -clock  CLK [all_outputs]
set_input_delay 0.100 -max -clock  CLK [remove_from_collection [all_inputs] [get_ports CLK]]

set_ideal_network [get_ports CLK]
set_clock_transition 0.100 [all_clocks]

current_design TopModule

set compile_auto_ungroup_override_wlm true
set_fix_multiple_port_nets -feedthroughs -outputs -constants

set strategy "timing_high_effort_script"
group_path -from [all_inputs ] -name IN2REG
group_path -to [all_outputs ] -name REG2OUT
group_path -from [all_inputs ] -to [all_outputs ] -name FEEDTHRUS
#set_wire_load_model -name "wl0"
#set wire_load_mode "top"
redirect ./Report_Files/compile1.log {compile_ultra -timing_high_effort_script} -tee; #initial compile command for high timing optimization
#redirect ./Report_Files/compile1.log {compile -map_effort high -area_effort high} -tee;
#set_wire_load_model -name "wl0"
#set wire_load_mode "top"
#ungroup -flatten -all

redirect ./Report_Files/TopModule_timing.rpt {report_timing -max 5 -capacitance -transition_time -nets -input_pins} -tee;
redirect ./Report_Files/TopModule_area.rpt {report_area} -tee;
redirect ./Report_Files/TopModule_qor.rpt {report_qor} -tee;
redirect ./Report_Files/TopModule_power.rpt {report_power} -tee;
redirect ./Report_Files/TopModule_constraints_pass_com1.rpt {report_constraints} -tee;

write -f verilog -hier -o ./Report_Files/TopModule_netlist.v
write_sdc ./Report_Files/TopModule.sdc
write_sdf ./Report_Files/TopModule.sdf
write -f ddc -h -o ./Report_Files/TopModule.ddc
report_net_fanout -high_fanout > Report_Files/fanout_report
report_design > Report_Files/design_report
write_saif -output Report_Files/TopModule.saif

#exit
