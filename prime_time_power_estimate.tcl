
set search_path [list . <path_to_folder_containing_.lib_and_.db_files> ]
set target_library [list <library.db>]
set link_library  [list * <library.db>]

read_verilog ~/verilog_netlist_path/file_netlist.v
current_design TopModule
link

set CLKIN_period 0.5


set power_enable_analysis true
set power_domains_compatibility true
set power_report_leakage_breakdowns true
set power_enable_multi_rail_analysis true

create_clock -name CLK -period $CLKIN_period [ get_ports {CLK}]

#source all_registers.tcl
set_switching_activity -toggle_count 1 -period 1.0 -static 0.5 [all_inputs]

create_power_rail_mapping VDD -lib vdd -cells [get_cells *]
create_power_rail_mapping VSS -lib vss -cells [get_cells *]
create_power_rail_mapping VBBPW -lib vbbpw -cells [get_cells *]
create_power_rail_mapping VBBNW -lib vbbnw -cells [get_cells *]


current_design TopModule

#create_power_group -name lvt_cells [get_cells -hierarchical -filter "ref_name =~ *_lvt"]

current_power_rail VDD
update_power
report_power -verbose > multivt_v7/S_65_1/vdd_power.txt

current_power_rail VBBNW
update_power
report_power -verbose > multivt_v7/S_65_1/vbbnw_power.txt

current_power_rail VBBPW
update_power
report_power -verbose > multivt_v7/S_65_1/vbbpw_power.txt

current_power_rail all
update_power
report_power -verbose > Report_Files_32buff/all_power_50.txt

exit
