# Cadence Encounter(R) RTL Compiler
#   version v07.20-s021_1 (64-bit) built Apr 17 2008
#


set_attribute lib_search_path ~/path_to_libraries_folder
set_attribute library {library.lib} 
read_hdl -v2001 list_of_verilog_files.v 
elaborate
read_vcd -vcd_module topology -module mkTopology -static paternoster_8buff_15.vcd 
report power -build_power_models
report power > paternoster_8buff_15_report
exit
