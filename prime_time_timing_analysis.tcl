

set SCRIPT_PATH [getenv SCRIPT_PATH]

source -echo -verbose ${SCRIPT_PATH}dc_setup.tcl;

echo "In pt.tcl - dc_setup.tcl is sourced.."
echo "|"
echo "|"
echo "|"
echo "|"
echo "|"
echo "|"
echo "|"
echo "|"
echo "|"


#to read netlist created at operating condition o1 while using .lib at o2
#read_ddc -netlist_only ${SCRIPT_PATH}/arm1176core_pass_only_timing_-40c_0.92v.ddc 


#read_ddc ${SCRIPT_PATH}/arm1176core_pass_only_timing_-40c_0.92v.ddc

#Routed netlist
#read_verilog ${SCRIPT_PATH}/A1176Core_routed_netlist_@-40_0.92.v
read_verilog ${SCRIPT_PATH}/netlist.v
current_design TopModule
link
#RC parasitics to use after Routing
read_parasitics ${SCRIPT_PATH}/SPEF/rc.spef.-40.postRoute -format SPEF


set hdlin_check_no_latch true
set hdlin_report_syn_cell true ; # no man entry for this command. Need to check
set hdlin_report_inferred_modules verbose
set hlo_resource_allocation "constraint_driven"

set report_default_significant_digits [getenv SIGDIGIT];

# set_flatten true -minimize single_output -phase true;

set auto_wire_load_selection "false"
set compile_delete_unloaded_sequential_cells "false"
set compile_seqmap_enable_output_inversion "false"
set compile_seqmap_propagate_constants "false"
set compile_seqmap_propagate_high_effort "false"
set timing_enable_multiple_clocks_per_reg "true"
set compile_dont_use_dedicated_scanout 1
set enable_recovery_removal_arcs "false"
set compile_implementation_selection "true"
set write_name_nets_same_as_ports true
set verilogout_higher_designs_first true
set verilogout_no_tri true
set report_default_significant_digits 4
#This version contains shift register identification, which will only replace the first element in a shift register with a mux scan FF, and use non-scan FFs for the remaining elements. Since this capability has not yet been qualified, TI recommends that you set the following 2 variables to disable this behaviour
#set compile_seqmap_identify_shift_registers  false

### By default DC 2008.09 does not use separate rise - fall capacitance - To enable this use this config 
#set timing_use_enhanced_capacitance_modeling true


#### commented by Arijit - Obsolete command
#set hdlin_enable_vpp true

set rm_icg_name "integrated:[getenv CLKGATINGCELL]" ;# Name of ICG cell


# -----------------------------------------------------------------------------
# Define the clock gating style.
# -----------------------------------------------------------------------------

set hdlin_no_group_register true

# set_clock_gating_style -sequential_cell latch -max_fanout 64     \
                       -positive_edge_logic $rm_icg_name         \
                       -control_point before -minimum_bitwidth 8 \
                       -control_signal scan_enable


# Set isolate ports
# set_isolate_ports -type buffer [all_outputs]
# set_fix_multiple_port_nets -feedthroughs -outputs -constants [get_designs *];
# set_structure -boolean true -boolean_effort high -timing true;

#insert_clock_gating ; #added compile_ultra -gate_clock

#set CLKIN_period 2000
set CLKIN_period [expr [getenv PERIOD] * [getenv TIME_MULT]]


#--------------------------------------------------------------------
#Setting Uncertainaties and Derates
#--------------------------------------------------------------------

set dpll_uncertainty          [expr $CLKIN_period * 0.05] ; ####### 5% of clock period
set hold_uncertainty          [expr $CLKIN_period * 0.0125] ; ####### 1.25% of clock period
set ocv_setup_uncertainty     [expr $CLKIN_period * 0.05]

 set_timing_derate -early 1.0
 set_timing_derate -late -cell_delay -clock [getenv TIMING_DERATE_CLOCK_LATE]
 set_timing_derate -late -cell_delay -data [getenv TIMING_DERATE_DATA_LATE]
 set_timing_derate -early -clock [getenv TIMING_DERATE_CLOCK_EARLY]
 set_timing_derate -late  1.0

create_clock -name CLK -period $CLKIN_period [ get_ports {CLK}]

set total_uncertainty [expr $dpll_uncertainty + $ocv_setup_uncertainty]
set_clock_uncertainty -setup $total_uncertainty [all_clocks]
set_clock_uncertainty -hold ${hold_uncertainty} [all_clocks]

#set CLKIN50pc [expr 0.5*$CLKIN_period]; ######### taking i/o delay from user 

## Set IO delays

#set_output_delay [expr [getenv OUTPUT_DELAY] * [getenv TIME_MULT]] -max -clock  CLK [all_outputs]
#set_input_delay  [expr [getenv INPUT_DELAY] * [getenv TIME_MULT]] -max -clock  CLK [remove_from_collection [all_inputs] [get_ports CLK]]
##Added by Archana: to remove the variable dependency.
#set_output_delay [expr [getenv PERIOD] / 5] -max -clock  CLK [all_outputs]
set_output_delay 150 -max -clock  CLK [all_outputs]
set_input_delay [expr [getenv PERIOD] / 5] -max -clock  CLK [remove_from_collection [all_inputs] [get_ports CLK]]

## set ideal nets
#set_ideal_net [list nRESETS1]
#set_ideal_net [list SE]
set_ideal_network [list SE] -no_propagate
#set_ideal_net [list nPORESET ]
#set_ideal_net [list nDBGTAPSMRST ]
#set_ideal_net [list nTestLogicRst ]

## set ideal networks
#set_ideal_network [get_ports nRESETS1]
set_ideal_network [get_ports SE]
#set_ideal_network [get_ports nPORESET ]
#set_ideal_network [get_ports nDBGTAPSMRST ]
#set_ideal_network [get_ports nTestLogicRst ]

set_ideal_network [get_ports CLK]
set_clock_transition [getenv SET_MAX_TRANSITION] [all_clocks]

set_multicycle_path 2 -setup -from SE
set_multicycle_path 1 -hold  -from SE


current_design A1176Core

set_max_transition [expr [getenv SET_MAX_TRANSITION]]  [current_design]
set_max_fanout     [getenv FANOUT_LIMIT]    [current_design]
#set_load         [getenv OUT_PUT_PIN_LOAD]     [all_outputs]
#set_load         [load_of [get_lib_pins [get_object_name [get_lib_cells */[getenv INPUT_CELL_DRIVE]]]/* -filter "pin_direction==in"]]     [all_outputs]
set_load         [load_of [get_lib_pins [get_object_name [get_lib_cells */[getenv OUTPUT_CELL_DRIVE]]]/* -filter "pin_direction==in"]]     [all_outputs]
set_driving_cell -lib_cell [getenv INPUT_CELL_DRIVE] [all_inputs]

# set_operating_conditions -max [getenv PTV]

if {[getenv WIRELOADMODEL] == "ZERO_INTERCONNECT"} {
	set_zero_interconnect_delay_mode true
 } else {
	set_wire_load_model -name [getenv WIRELOADMODEL]
 }

set wire_load_mode "top"
set compile_auto_ungroup_override_wlm true


##Adding derates (for SR80lx to meet with vendor libs)
#set_timing_derate 1.23 -cell_delay -cell_check -net_delay

# -----------------------------------------------------------------------------
# Setup the compile options
# -----------------------------------------------------------------------------

# set_fix_multiple_port_nets -feedthroughs -outputs -constants

set compile_dont_use_dedicated_scanout 1

if {[getenv COMPILE_STRATEGY] == "TIMING"} {
	set strategy "timing_high_effort_script"
}
if {[getenv COMPILE_STRATEGY] == "AREA"} {
	set strategy "area_high_effort_script"
	set_max_area 0 -ignore_tns
}

if {![file exists [getenv REPORT_DIR]]} {
	sh mkdir -p [getenv REPORT_DIR]
}

set reportDir [getenv REPORT_DIR]
set compile_log_format {%elap_time %area %wns %tns %drc %endpoint %max_delay %trials %min_delay}

source /sp/dftm/Activities/IITM/Virat/TCL/snps_myprocs.tcl
source /sp/dftm/Activities/IITM/Virat/TCL/DC_TCLs/get_cell_usage.tcl

group_path -from [all_inputs ] -name IN2REG
group_path -to [all_outputs ] -name REG2OUT
group_path -from [all_inputs ] -to [all_outputs ] -name FEEDTHRUS

#write_sdf ${SCRIPT_PATH}delay@-40_1.116.sdf
#report_timing -from uCoreDP/uMul/Bkwrds2CtlM2_reg -to uCoreDP/uMul/Sum4M3_reg[19] -capacitance -transition_time -nets -input_pins -through uCoreDP/uMul/Bkwrds2CtlM2  -through n56990 -through n56986 -through n56984 -through n83559 -through n83560 -through n83573 -through n83563 -through n83562 -through intadd_18/B[4] -through intadd_18/SUM[4]   -through uCoreDP/uMul/Sum4M2[19] > -40c_0.92v.rpt
#report_timing -from uCoreDP/uMul/Bkwrds2CtlM2_reg -to uCoreDP/uMul/Sum4M3_reg[19] -capacitance -transition_time -nets -input_pins -through U41570/a > -40c_0.92v.rpt
# set_critical_range [expr [getenv PERIOD] / 4]  [current_design]
#redirect compile_map_effort.log {compile -map_effort high} -tee;
# ungroup -flatten -all
#insert_dft
# redirect $reportDir/arm1176core_timing_pass_com1.rpt {report_timing -max 10 -capacitance -transition_time -nets -input_pins} -tee;
# report_timing > /sp/dftm/Activities/IITM/Virat/ARM11/gs80/runSyn/123.txt
# set_false_path -from [all_inputs]
# set_false_path -to [all_outputs]
#redirect ${SCRIPT_PATH}/check.timing {report_timing -capacitance -transition_time -nets -input_pins \
[get_timing_paths -max 500 -nworst 1 -slack_lesser_than 56 -group CLK]}
#write_spice_deck -output ${SCRIPT_PATH}multiple_path_EXP/clk/clk.hsp -ground_coupling_capacitors -sub_circuit_file ${SCRIPT_PATH}cells.hspice \
[get_timing_paths -max 500 -nworst 1 -slack_lesser_than 56 -group CLK]
# report_timing -from uCoreDP/uMul/Sum4M3_reg[3] -to uCoreDP/uMul/iMulOutWr_reg[26] -transition_time -nets -input_pins
#write_spice_deck -output ${SCRIPT_PATH}trial/xxx.hsp -ground_coupling_capacitors -sub_circuit_file ${SCRIPT_PATH}cells.hspice \
[get_timing_paths -from uCoreDP/uMul/Sum4M3_reg[3] -to uCoreDP/uMul/iMulOutWr_reg[26]]
#source ${SCRIPT_PATH}/paths.tcl
#source ${SCRIPT_PATH}/paths@125.tcl
#source ${SCRIPT_PATH}/83_paths.tcl
#report_net ${SCRIPT_PATH}/report_net_@125.rpt
#source ${SCRIPT_PATH}/paths_with_through.tcl
#redirect ${SCRIPT_PATH}interconnect_modeling/500paths_@125.rpt {report_timing -capacitance -transition_time -nets -input_pins -nosplit \
[get_timing_paths -max 500 -nworst 1 -group CLK]}
#source ${script_path}interconnect_modeling/5paths.tcl
#source ${SCRIPT_PATH}interconnect_modeling/pin.total.tcl
#only for gate delay
#source ${SCRIPT_PATH}interconnect_modeling/2paths.tcl
#report_power >> ${SCRIPT_PATH}leakage.txt
#source ${SCRIPT_PATH}interconnect_modeling/796paths/796pathslist.tcl

#for strong silicon
#source ${SCRIPT_PATH}interconnect_modeling/strong_silicon/789pathlist_S.tcl
#source ${SCRIPT_PATH}interconnect_modeling/strong_silicon/generate_hspice.tcl
#source ${SCRIPT_PATH}interconnect_modeling/strong_silicon/hspice_lvt_v2_first_few.tcl
#source ${SCRIPT_PATH}interconnect_modeling/strong_silicon/823pathlist_S_lvt.tcl
#redirect ${SCRIPT_PATH}interconnect_modeling/strong_silicon/multivt_D0_v8_85per/500_125_1.1_lvt.rpt {report_timing -capacitance -transition_time -nets -input_pins -nosplit [get_timing_paths -max 500 -nworst 1 -group CLK]}
source ${SCRIPT_PATH}interconnect_modeling/strong_silicon/multivt_D0_v8_85per/generate_hspice.tcl

#for weak silicon
#source ${SCRIPT_PATH}interconnect_modeling/weak_silicon/791pathslist.tcl
#source ${SCRIPT_PATH}interconnect_modeling/weak_silicon/generate_hspice.tcl
#source ${SCRIPT_PATH}interconnect_modeling/weak_silicon/multivt_D0_v8_85per/1000pathslist.tcl
#source ${SCRIPT_PATH}interconnect_modeling/weak_silicon/multivt_D0_v8_85per/generate_hspice.tcl
#redirect ${SCRIPT_PATH}interconnect_modeling/weak_silicon/multivt_D0_v8_85per/500_-40_0.814_lvt.rpt {report_timing -capacitance -transition_time -nets -input_pins -nosplit [get_timing_paths -max 500 -nworst 1 -group CLK]}

#for nominal silicon
#source ${SCRIPT_PATH}interconnect_modeling/nominal_silicon/generate_hspice.tcl
#redirect ${SCRIPT_PATH}interconnect_modeling/nominal_silicon/multivt_D0_v8_85per/800paths_@125_1.144_0.9.rpt {report_timing -capacitance -transition_time -nets -input_pins -nosplit [get_timing_paths -max 800 -nworst 1 -group CLK]}
#source ${SCRIPT_PATH}interconnect_modeling/nominal_silicon/multivt_D0_v8_85per/generate_hspice.tcl


#source ${SCRIPT_PATH}interconnect_modeling/timing_reports_@diff_corners/791pathslist.tcl
#increase precision
#set report_default_significant_digits 4
#for power analysis
#set power_enable_analysis true
#report_power -hierarchy -levels 1 -nosplit >> ${SCRIPT_PATH}/power_reports/temp/S_65_1.3
#for power analysis
#set power_enable_analysis true
#set power_report_leakage_breakdowns true
#set power_enable_multi_rail_analysis true
#set power_domains_compatibility true
#create_clock -name CLK -period $CLKIN_period [ get_ports {CLK}]
#create_power_rail_mapping VDD -lib vdd -cells [get_cells *]
#create_power_rail_mapping VSS -lib vss -cells [get_cells *]
#create_power_rail_mapping VBBPW -lib vbbpw -cells [get_cells *]
#create_power_rail_mapping VBBNW -lib vbbnw -cells [get_cells *]
#set power_analysis_mode averaged
#current_power_rail VDD
#update_power
#report_power > ${SCRIPT_PATH}temp
#report_power > ${SCRIPT_PATH}power_reports/using_rails/105_1.25_VDD
#current_power_rail VSS
#update_power
#report_power > ${SCRIPT_PATH}power_reports/using_rails/105_1.25_VSS
#current_power_rail VBBPW
#update_power
#report_power > ${SCRIPT_PATH}power_reports/using_rails/105_1.25_VBBPW
#current_power_rail VBBNW
#update_power
#report_power > ${SCRIPT_PATH}power_reports/using_rails/105_1.25_VBBNW


#source ${SCRIPT_PATH}interconnect_modeling/logic_delay_modeling/generate_hspice_with_unique_endpoint.tcl


#source ${SCRIPT_PATH}power_reports/generate_hspice.tcl


